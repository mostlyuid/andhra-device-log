# Introduction
This is a repository which contains the Raw Dump of Aadhaar authentication records from Andhra. 

It is scrapped out from [the benefit portal](http://tspost.aponline.gov.in/PostalWebPortal/UserInterface/ReportsMain_TG.aspx).

# Analysis
Analysis is only possible via a Big Data program and that will be added 
soon enough. 

# Raw Data
Raw data is available in the "raw-data" folder. 

# Data Format
There are 10 data fields in the CSV file. Consider a typical row
M.BALASIDDULU,0620900002db5345,549110,SSP,-,201606010722386570620900002db5345,01/06/2016 07:22:31,1FA,01/06/2016 07:22:47,ASA Server/Link failure

## Field 1 
It is the name of the operator who owns the device: M.BALASIDDULU. For the 
purpose of failure analysis, it is irrelevant and can be ignored.

## Field 2 
It is the time stamp of the authentication: 0620900002db5345. This can be 
ignored as it is available elsewhere.

## Field 3
It is the transient ID number of the transaction: 549110. For a given person, 
it stays constant for the purpose of the transaction. Say, they try to 
authenticate "n", times, the same transient ID number will be used. This will 
allow us to compute the number of times, a beneficiary attempts to authenticate
a given day, and the total success and failure attempts. 

## Field 4
It is the name of the scheme, for which authentication was performed: SSP
* SSP is Social Security Pension
* MGNREGA is the Mahatma Gandhi National Rural Employment Guarantee Act
* VRO is the Village Revenue Officer 

VRO attempts are overrides which are done by the Block officers to clear 
payments for those whom, biometric authentication does not work. They are 
typically interspersed within authentication attempts and hence typically 
need more work to map to exclusion faced by beneficiaries. 

## Field 5
It is almost always a "-" and can be ignored.

## Field 6
It is a combination of date and timestamp: 201606010722386570620900002db5345. 
As it is available in a better format in other fields, it can be safely 
ignored. 

## Field 7
It is date and time, when the authentication request was sent: 
01/06/2016 07:22:31

## Field 8
It is the number of attempts to get through. 
* 1FA means 1st Attempt
* 2FA means 2nd Attempt
* 3FA means 3rd Attempt
* BFD means Best Finger Detection

## Field 9 
It is date and time, when the authentication request was responded to:
01/06/2016 07:22:47

## Field 10
It is the response for the authentication request: ASA Server/Link failure
The full list of error codes are published and available by UIDAI at: 
http://164.100.129.6/netnrega/progofficer/error_desc.htm

# Steps to compute impacted beneficiaries

## Modeling Transactions
The purpose of a MGNREGA or SSP transaction is to prove to the government that
the Aadhaar number and the beneficiary is genuine, for receiving payment (for
work done) or for getting pension (SSP).

Hence multiple transactions on the same day is quite possible, since authentication
for every month is mandatory. However every transaction may require multiple 
attempts and may also need VRO clearance. This means it is important to convert
raw authentication failures to transaction success or failures. 

So the first step is to map authentications to transactions through a mapping 
step.



